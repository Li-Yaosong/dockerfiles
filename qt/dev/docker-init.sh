#!/bin/bash
cd /home
if [ -f "/home/qt-archive.tar.gz" ]; then
    echo "Unzip Qt, please wait!"
    tar -xzvf ./qt-archive.tar.gz > /dev/null 2>&1
    rm -rf ./qt-archive.tar.gz
    echo "Unzip Qt successful!"
fi
echo "Welcome to use!"
qmake -v
/bin/bash