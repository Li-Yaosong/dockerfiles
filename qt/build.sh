#!bin/bash
QT_VERSION=%1
if [$(uname -m) == "x86_64"]; then
    ARCH="amd64"
else
    ARCH=$(uname -m)
fi
docker build --build-arg QT_VERSION=${QT_VERSION} -t qt:${QT_VERSION}_${ARCH} .