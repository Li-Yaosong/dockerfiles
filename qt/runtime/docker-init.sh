#!/bin/bash
cd /home
if [ ${LANG} = "zh_CN.UTF-8" ]; then
    WAIT="正在解压Qt，请稍等！"
    SUCC="解压Qt成功！"
    WELCOME="欢迎使用！"
    NOTE="注意：当前系统语言为中文，如果需要切换为英文，请在启动容器时添加环境变量：-e LANG=en_US.UTF-8"
else
    WAIT="Unzip Qt, please wait!"
    SUCC="Unzip Qt successful!"
    WELCOME="Welcome to use!"
    NOTE="Note: The current system language is English.\nIf you need to switch to Chinese, please add environment variables when starting the container: -e LANG=zh_CN.UTF-8"
fi

if [ -f "/home/qt-archive.tar.gz" ]; then
    echo ${WAIT}
    tar -xzvf ./qt-archive.tar.gz > /dev/null 2>&1
    rm -rf ./qt-archive.tar.gz
    echo ${SUCC}
fi

echo ${WELCOME}
echo -e ${NOTE}
echo $QT_DIR
if [ -f "${TARGET}" ]; then
    ${TARGET}
else
    /bin/bash
fi