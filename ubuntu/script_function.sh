#！/bin/bash

machine=host
echo --$VERSION--
if [ "${VERSION}" = "18.04" ]; then
    NAME=bionic
elif [ "${VERSION}" = "20.04" ]; then
    NAME=focal
elif [ "${VERSION}" = "22.04" ]; then
    NAME=jammy
elif [ "${VERSION}" = "24.04" ]; then
    NAME=noble
    DEB822=True
else
    NAME="error"
fi

#判断架构
if [ "$(uname -m)" = "aarch64" ]; then
    ARCH=arm64
    UBUNTU_PATH=ubuntu-ports
else
    ARCH=amd64
    UBUNTU_PATH=ubuntu
fi
EXTERNAL_URL=http://mirrors.ustc.edu.cn/${UBUNTU_PATH}/
if [ "${machine}" = "host" ]; then
    BASE_URL=http://192.168.8.2/${UBUNTU_PATH}/
else
    BASE_URL=${EXTERNAL_URL}
fi
echo BASE_URL:${BASE_URL}
#写一个函数
update()
{
    chroot ./ubuntu-root apt-get update
    chroot ./ubuntu-root apt-get upgrade -y
    chroot ./ubuntu-root apt-get autoremove -y
    chroot ./ubuntu-root apt-get clean
}
clean()
{
    rm -rf ./ubuntu-root/var/lib/apt/lists/* \
           ./ubuntu-root/tmp/* \
           ./ubuntu-root/var/tmp/* \
           ./ubuntu-root/root/.cache \
           ./ubuntu-root/var/cache/apt/archives/*.deb \
           ./ubuntu-root/var/cache/apt/*.bin \
           ./ubuntu-root/var/lib/apt/lists/* \
           ./ubuntu-root/usr/share/*/*/*/*.gz \
           ./ubuntu-root/usr/share/*/*/*.gz \
           ./ubuntu-root/usr/share/*/*.gz \
           ./ubuntu-root/usr/share/doc/*/README* \
           ./ubuntu-root/usr/share/doc/*/*.txt \
           ./ubuntu-root/usr/share/locale/*/LC_MESSAGES/*.mo 
}

setup()
{
    cleanup
    #创建文件夹,下载基础镜像
    echo -----${NAME}-----
    # mirrors.ustc.edu.cn
    debootstrap --variant=minbase ${NAME} ./ubuntu-root ${BASE_URL}
    #换源
    if [ "${DEB822}" = "True" ]; then
        chsources_deb822
    else
        chsources
    fi
    #更新
    update
    #docker build
    clean
    if [ "${DEB822}" = "True" ]; then
        externalsources_deb822
    else
        externalsources
    fi
    #修改权限
    chown -R ${USR}:${GROUP} ./ubuntu-root
}
build()
{
    #docker build
    docker build . -t liyaosong/ubuntu:${VERSION}-${ARCH}
}
push()
{
    #docker push
    docker push liyaosong/ubuntu:${VERSION}-${ARCH}
}
# 替换源
chsources()
{
    echo "\
deb ${BASE_URL} ${NAME} main restricted universe multiverse
# deb-src ${BASE_URL} ${NAME} main restricted universe multiverse
deb ${BASE_URL} ${NAME}-updates main restricted universe multiverse
# deb-src ${BASE_URL} ${NAME}-updates main restricted universe multiverse
deb ${BASE_URL} ${NAME}-backports main restricted universe multiverse
# deb-src ${BASE_URL} ${NAME}-backports main restricted universe multiverse">./ubuntu-root/etc/apt/sources.list
}
externalsources()
{
    echo "\
deb ${EXTERNAL_URL} ${NAME} main restricted universe multiverse
# deb-src ${EXTERNAL_URL} ${NAME} main restricted universe multiverse
deb ${EXTERNAL_URL} ${NAME}-updates main restricted universe multiverse
# deb-src ${EXTERNAL_URL} ${NAME}-updates main restricted universe multiverse
deb ${EXTERNAL_URL} ${NAME}-backports main restricted universe multiverse
# deb-src ${EXTERNAL_URL} ${NAME}-backports main restricted universe multiverse">./ubuntu-root/etc/apt/sources.list
}

chsources_deb822()
{
    echo "\
Types: deb
URIs: ${BASE_URL}
Suites: ${NAME} ${NAME}-updates ${NAME}-backports ${NAME}-security
Components: main restricted universe multiverse
Signed-By: /usr/share/keyrings/ubuntu-archive-keyring.gpg

# Types: deb-src
# URIs: ${BASE_URL}
# Suites: ${NAME} ${NAME}-updates ${NAME}-backports ${NAME}-security
# Components: main restricted universe multiverse
# Signed-By: /usr/share/keyrings/ubuntu-archive-keyring.gpg">./ubuntu-root/etc/apt/sources.list.d/ubuntu.sources
    rm -rf ./ubuntu-root/etc/apt/sources.list
}
externalsources_deb822()
{
    echo "\
Types: deb
URIs: ${EXTERNAL_URL}
Suites: ${NAME} ${NAME}-updates ${NAME}-backports ${NAME}-security
Components: main restricted universe multiverse
Signed-By: /usr/share/keyrings/ubuntu-archive-keyring.gpg

# Types: deb-src
# URIs: ${EXTERNAL_URL}
# Suites: ${NAME} ${NAME}-updates ${NAME}-backports ${NAME}-security
# Components: main restricted universe multiverse
# Signed-By: /usr/share/keyrings/ubuntu-archive-keyring.gpg">./ubuntu-root/etc/apt/sources.list.d/ubuntu.sources
    rm -rf ./ubuntu-root/etc/apt/sources.list
}
cleanup()
{
    #判断文件夹是否存在
    if [ -d ./ubuntu-root ]; then
        rm -rf ./ubuntu-root
    fi
}
